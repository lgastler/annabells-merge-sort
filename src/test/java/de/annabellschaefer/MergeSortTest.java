package de.annabellschaefer;

import org.junit.Assert;
import org.junit.Test;


public class MergeSortTest {

    @Test
    public void mergesort() {
        MergeSort sort = new MergeSort();

        int arr1[] = {12, 15, 23, 4 , 6, 10, 35, 28}; //Even number of element
        int arr2[]={}; //Empty list
        int arr3[] = {4, 6, 10, 12, 15, 23, 28, 35}; //already sorted array
        int arr4[] = {12, 15, 23, 4 , 6, 10, 35}; //odd length array
        int arr5[] = {35, 28, 23, 15, 12, 10, 6, 4}; //descending sorted array input
        int arr6[] = {12};//one element
        int arr7[] = {12, 4}; // two elements
        int arr9[] = {12, 15, -23, -4 , 6, 10, -35, 28}; //negative elements
        int arr10[] = {12, 12, 23, 4 , 6, 6, 10, -35, 28}; //duplicate elements
        int arr11[] = {12, 12, 12, 12, 12}; //Same element

        int arrL1[] = {4,6,10,12,15,23,28,35};
        int arrL2[] = {};
        int arrL3[] = {4, 6, 10, 12, 15, 23, 28, 35};
        int arrL4[] = {4, 6, 10, 12, 15, 23, 35};
        int arrL5[] = {4, 6, 10, 12, 15, 23, 28, 35};
        int arrL6[] = {12};
        int arrL7[] = {4,12};
        int arrL9[] = {-35, -23, -4, 6, 10, 12, 15, 28};
        int arrL10[] = {-35, 4, 6, 6, 10, 12, 12, 23, 28};
        int arrL11[] = {12,12,12,12,12};


        // assert statements
        Assert.assertArrayEquals(arrL1, sort.mergeSortIt(arr1));
        Assert.assertArrayEquals(arrL2, sort.mergeSortIt(arr2));
        Assert.assertArrayEquals(arrL3, sort.mergeSortIt(arr3));
        Assert.assertArrayEquals(arrL4, sort.mergeSortIt(arr4));
        Assert.assertArrayEquals(arrL5, sort.mergeSortIt(arr5));
        Assert.assertArrayEquals(arrL6, sort.mergeSortIt(arr6));
        Assert.assertArrayEquals(arrL7, sort.mergeSortIt(arr7));
        Assert.assertArrayEquals(arrL9, sort.mergeSortIt(arr9));
        Assert.assertArrayEquals(arrL10, sort.mergeSortIt(arr10));
        Assert.assertArrayEquals(arrL11, sort.mergeSortIt(arr11));



    }
}