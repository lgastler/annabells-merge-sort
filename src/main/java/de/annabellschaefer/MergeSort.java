package de.annabellschaefer;

import java.util.concurrent.ThreadLocalRandom;

public class MergeSort {

    public static void main(String[] args) {

        //creating Array for Test numbers
        int k = 10;
        int[] arr = new int[k];
        //fill test array with random numbers
        for(int i = 0; i <= k-1; i++){
            int randomNum = ThreadLocalRandom.current().nextInt(0, 10);
            arr[i] = randomNum;
        }

        //print unsorted array
        System.out.printf("Unsorted Array is \n");
        printArray(arr);

        //get and print sorted array
        System.out.printf("\nSorted array is \n");
        printArray(mergeSortIt(arr));

    }


    public static int[] mergeSortIt(int[] a){

        int n = a.length;
        int size;
        int leftIndex;

        //size of merged Arrays will get larger over time
        for(size = 1; size <= n-1; size = 2*size){

            for(leftIndex = 0; leftIndex<n-1; leftIndex += 2*size){
                int midIndex = leftIndex + (size-1);

                //get min value of 2 values
                int rightIndex;
                if ((leftIndex + 2*size -1) < n-1){
                    rightIndex = leftIndex + 2*size -1;
                } else {
                    rightIndex = n-1;
                }

                if(!(midIndex > rightIndex)) {
                    merge(a, leftIndex, midIndex, rightIndex);
                }
            }
        }

        return a;

    }


    public static int[] merge(int[] arr, int left, int mid, int right){

        //length of starting arrays
        int i1 = mid - left + 1;
        int i2 = right - mid;


        //create temp arrays
        int[] Left = new int[i1];
        int[] Right = new int[i2];


        //copy left side of array
        for (int i = 0; i < i1; i++){
            Left[i] = arr[left+i];
        }

        //copy right side of array
        for (int j = 0; j < i2; j++){
            Right[j] = arr[mid + 1 + j];
        }

        //fill arr with merged items
        int i = 0;
        int j = 0;
        int k = left;
        while(i < i1 && j < i2){

            if(Left[i] <= Right[j]){
                arr[k++] = Left[i++];
            } else {
                arr[k++] = Right[j++];
            }
        }

        //remaining of left
        while (i < i1){
            arr[k++] = Left[i++];
        }

        //remaining of right
        while(j < i2){
            arr[k++] = Right[j++];
        }

        return arr;

    }

    //helper method to print array
    static void printArray(int A[])
    {
        int size = A.length;
        int i;
        for (i=0; i < size; i++)
            System.out.printf("%d ", A[i]);
        System.out.printf("\n");
    }
}
